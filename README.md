CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Features
 * Credits


INTRODUCTION
------------

This module provides a new field for "Language Combinations" that
defines a source language and a target language. Example: the field can
be used for users to register their translation skills.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Go to configuration page of eny entity such as users or content type
(admin/structure/types). Select "Manage fields" and then "Add fields",
then select the field named "Language Combination".


FEATURES
--------

The language combination field has 2 different field formatters:
 * Default
 * Table


CREDITS
-------

Code originally grabbed from Translation Language abilities
subdmoule in Translation Management Tool
 * https://www.drupal.org/project/tmgmt

language_combination 8.x-1.x by developmenticon
