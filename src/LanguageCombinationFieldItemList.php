<?php

namespace Drupal\language_combination;

use Drupal\Core\Field\FieldItemList;

/**
 * Defines an item list class for language combination fields.
 */
class LanguageCombinationFieldItemList extends FieldItemList {

  /**
   * Get source language values.
   *
   * @return array
   *   Array of all the source langcodes.
   */
  public function getValueSourceLangcodes() {
    return $this->getValueLangcodes(['language_source']);
  }

  /**
   * Get target language values.
   *
   * @return array
   *   Array of all the target langcodes.
   */
  public function getValueTargetLangcodes() {
    return $this->getValueLangcodes(['language_target']);
  }

  /**
   * Get all language values.
   *
   * @return array
   *   Array of all the langcodes.
   */
  public function getValueAllLangcodes() {
    return $this->getValueLangcodes(['language_source', 'language_target']);
  }

  /**
   * Get language value which depends on keys.
   *
   * @param array $keys
   *   Selected attribute keys, either language_source or language_target.
   *
   * @return array
   *   Array of all the selected langcodes.
   */
  private function getValueLangcodes(array $keys) {
    $languages = [];

    foreach ($this->getValue() as $values) {
      foreach ($keys as $key) {
        array_push($languages, $values[$key]);
      }
    }

    return array_unique($languages);
  }

}
